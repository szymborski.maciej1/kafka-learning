package com.demo.learningkafka.topic;

import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;
import org.springframework.kafka.core.KafkaAdmin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Configuration
public class KafkaTopicConfig {

    @Value("${spring.kafka.producer.bootstrap-servers}")
    private List<String> bootstrapServers;

    @Bean
    public KafkaAdmin admin() {
        Map<String, Object> configs = new HashMap<>();
        configs.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, String.join(",", bootstrapServers));
        return new KafkaAdmin(configs);
    }

    @Bean
    public NewTopic firstTopic() {
        return TopicBuilder.name("firstTopic")
                .build();
    }

    @Bean
    public NewTopic secondTopic() {
        return TopicBuilder.name("secondTopic")
                .build();
    }

    @Bean
    public NewTopic thirdTopic() {
        return TopicBuilder.name("thirdTopic")
                .build();
    }
}
