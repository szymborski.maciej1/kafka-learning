package com.demo.learningkafka.listener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class KafkaListeners {

    @KafkaListener(topics = "firstTopic", groupId = "group")
    void firstTopicListener(String data) {
        log.info("Received data on firstTopic - {}",data);
    }

    @KafkaListener(topics = "secondTopic", groupId = "group")
    void secondTopicListener(String data) {
        log.info("Received data on secondTopic - {}",data);
    }

    @KafkaListener(topics = "thirdTopic", groupId = "group")
    void thirdTopicListener(String data) {
        log.info("Received data on thirdTopic - {}",data);
    }
}
