package com.demo.learningkafka.producer.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class KafkaProducerController {

    private final KafkaTemplate<String,String> kafkaTemplate;

    @PostMapping("/first-topic")
    public ResponseEntity<String> sendMessageToFirstTopic(@RequestParam String message) {
        kafkaTemplate.send("firstTopic",message);
        return new ResponseEntity<>("Message to First Topic successfully sent!", HttpStatus.OK);
    }

    @PostMapping("/second-topic")
    public ResponseEntity<String> sendMessageToSecondTopic(@RequestParam String message) {
        kafkaTemplate.send("secondTopic",message);
        return new ResponseEntity<>("Message to Second Topic successfully sent!", HttpStatus.OK);
    }

    @PostMapping("/third-topic")
    public ResponseEntity<String> sendMessageToThirdTopic(@RequestParam String message) {
        kafkaTemplate.send("thirdTopic",message);
        return new ResponseEntity<>("Message to Third Topic successfully sent!", HttpStatus.OK);
    }
}
